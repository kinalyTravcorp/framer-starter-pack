(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var btnAfter, btnBefore, i, j, mainContainer, mainContainerArray, numberOfPage, page, pageArray, pageItem, palette, ref;

palette = ['#f2eeb1', '#e1edce', '#b7d0c1', '#8ab3b3', '#668391', '#3f5270', '#6e4c60', '#9d4651', '#cf3e40', '#d66b5f', '#df967c', '#e8c198'];

page = new PageComponent({
  width: Screen.width,
  height: Screen.height,
  scrollVertical: false,
  backgroundColor: 'white'
});

page.content.draggable.enabled = false;

numberOfPage = 2;

pageArray = [];

mainContainerArray = [];

for (i = j = 0, ref = numberOfPage - 1; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
  pageItem = new Layer({
    parent: page.content,
    width: Screen.width,
    height: Screen.height,
    x: Screen.width * i,
    backgroundColor: Utils.randomChoice(palette),
    name: i
  });
  pageItem.scroll = true;
  pageItem.scrollHorizontal = false;
  pageItem.mouseWheelEnabled = true;
  pageArray.push(pageItem);
  mainContainer = new Layer({
    width: 800,
    height: 200,
    backgroundColor: 'white',
    superLayer: pageArray[i]
  });
  mainContainer.centerY();
  mainContainer.style = {
    width: '800px',
    marginLeft: 'calc((100% - 800px) / 2)'
  };
  mainContainer.scroll = true;
  mainContainer.scrollHorizontal = false;
  mainContainer.mouseWheelEnabled = true;
  mainContainerArray.push(mainContainer);
  btnBefore = new Layer({
    backgroundColor: palette[10],
    width: 120,
    height: 40,
    superLayer: mainContainerArray[i]
  });
  btnBefore.fluid({
    yAlign: 'bottom',
    yOffset: -10,
    xAlign: 'left',
    xOffset: 10
  });
  btnBefore.onClick(function() {
    var previous;
    previous = page.currentPage.name - 1;
    if (previous !== -1) {
      return page.snapToPage(pageArray[previous], false);
    }
  });
  btnAfter = new Layer({
    backgroundColor: palette[10],
    width: 120,
    height: 40,
    superLayer: mainContainerArray[i]
  });
  btnAfter.fluid({
    yAlign: 'bottom',
    yOffset: -10,
    xAlign: 'right',
    xOffset: -10
  });
  btnAfter.onClick(function() {
    return page.snapToNextPage('right', false);
  });
}

Events.wrap(window).addEventListener("resize", function(event) {
  var k, ref1, results;
  page.width = Screen.width;
  page.height = Screen.height;
  results = [];
  for (i = k = 0, ref1 = numberOfPage - 1; 0 <= ref1 ? k <= ref1 : k >= ref1; i = 0 <= ref1 ? ++k : --k) {
    pageArray[i].width = Screen.width;
    pageArray[i].height = Screen.height;
    results.push(pageArray[i].x = Screen.width * i);
  }
  return results;
});


},{}]},{},[1]);

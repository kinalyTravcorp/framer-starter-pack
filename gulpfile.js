/*!
 * gulp
 * to install locally
 * $ npm install gulp gulp-sketch gulp-coffee gulp-notify gulp-util coffeeify browser-sync vinyl-source-stream watchify browserify --save-dev
 *
 * to use global packages
 * $ npm link gulp gulp-sketch gulp-notify gulp-util coffeeify browser-sync vinyl-source-stream watchify browserify
 */

// Load plugins
 var gulp = require('gulp'),
     sketch = require('gulp-sketch'),
     gutil = require('gulp-util'),
     notify = require('gulp-notify'),
     browserify = require('browserify'),
     watchify = require('watchify'),
     sourcestream = require('vinyl-source-stream'),
     browserSync = require('browser-sync');


// Paths and config variables
var source = './_src',
		prod = './_dist';


// Export Sketch assets
gulp.task('sketch', function() {
  return gulp
  	.src(source + '/*.sketch')
    .pipe(sketch({
      export: 'slices',
      format: 'png',
      saveForWeb: true,
      scales: 1.0,
      trimmed: false
    }))
    .pipe(gulp.dest(prod + '/img'))
    .pipe(notify({ message: 'Sketch task complete' }));
})



// Copy
gulp.task('copy', function() {
  gulp
    .src(source + '/index.html')
    .pipe(gulp.dest(prod))
    .pipe(notify({ message: 'HTML copied' }))  
  gulp
    .src(source + '/lib/**/*')
    .pipe(gulp.dest(prod + '/lib'))
    .pipe(notify({ message: 'Lib copied' }))
  gulp
    .src(source + '/img/**/*.{png, jpg, svg}')
    .pipe(gulp.dest(prod + '/img'))
    .pipe(notify({ message: 'img folder copied' }));
})



// Watch task
var browserifyOpts = {
  extensions: ['.coffee'],
  transform: ['coffeeify']
};

gulp.task('watch', function() {
  gulp.watch(source + '/*.sketch', ['sketch'])

  var bundler = watchify(browserify(source + '/app.coffee', browserifyOpts));

  bundler.on('update', rebundle);

  function rebundle() {
    console.log('Bundling...');

    return bundler.bundle()
      .on('error', function(e) {
        gutil.log('Browserify error', e);
      })
      .pipe(sourcestream('app.js'))
      .pipe(gulp.dest(prod));
  }

  rebundle();

  browserSync({
    server: {
      baseDir: prod
    },
    // browser: 'google chrome',
    injectChanges: false,
    files: [prod + '/**/*.*']
    // notify: false
  })
})


// Build task
gulp.task('build', function() {
    gulp.start('copy','sketch');
});


// Default task
gulp.task('default', function() {
    gulp.start('build','watch');
});
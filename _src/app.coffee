# palette
palette = [
  '#f2eeb1', # [0] pale yellow
  '#e1edce', # [1] pale green
  '#b7d0c1', # [2] light green
  '#8ab3b3', # [3] aqua green
  '#668391', # [4] blue-ish green
  '#3f5270', # [5] blue denim
  '#6e4c60', # [6] purple
  '#9d4651', # [7] maroon purple
  '#cf3e40', # [8] firy red
  '#d66b5f', # [9] muted red
  '#df967c', # [10] salmon orange
  '#e8c198' # [11] pale orange
]

# define a page component
page = new PageComponent
  width: Screen.width
  height: Screen.height
  scrollVertical: false
  backgroundColor: 'white'

# disable draggable page
page.content.draggable.enabled = false


# set the number of page required for the prototype (not necessarily screens)
numberOfPage = 2


# array definition to store and target pages and containers
pageArray = []
mainContainerArray = []



# loop to create the pages, the containers and any other common elements
for i in [0..numberOfPage-1]
  # create the page layers that are linked to the page component
  pageItem = new Layer
    parent: page.content
    width: Screen.width
    height: Screen.height
    x: Screen.width * i
    backgroundColor: Utils.randomChoice(palette)
    name: i

  pageItem.scroll = true
  pageItem.scrollHorizontal = false
  pageItem.mouseWheelEnabled = true

  pageArray.push(pageItem)



  # create the main containers for each page
  mainContainer = new Layer
    width: 800
    height: 200
    backgroundColor: 'white'
    superLayer: pageArray[i]

  mainContainer.centerY()

  mainContainer.style =
    width: '800px',
    marginLeft: 'calc((100% - 800px) / 2)'

  # make the container scrollable
  mainContainer.scroll = true
  mainContainer.scrollHorizontal = false
  mainContainer.mouseWheelEnabled = true

  mainContainerArray.push(mainContainer)



  # define nav for each page
  btnBefore = new Layer
    backgroundColor: palette[10]
    width: 120
    height: 40
    superLayer: mainContainerArray[i]

  btnBefore.fluid
    yAlign: 'bottom'
    yOffset: -10
    xAlign: 'left'
    xOffset: 10

  #interaction
  btnBefore.onClick ->
    # custom snapToPreviousPage as the function doesn't seem to work
    previous = page.currentPage.name - 1
    if previous != -1
      page.snapToPage(
        pageArray[previous]
        false
      )

  btnAfter = new Layer
    backgroundColor: palette[10]
    width: 120
    height: 40
    superLayer: mainContainerArray[i]

  btnAfter.fluid
    yAlign: 'bottom'
    yOffset: -10
    xAlign: 'right'
    xOffset: -10

  #interaction
  btnAfter.onClick ->
    page.snapToNextPage(
      'right',
      false
    )



# resize update : probably not good for performance
Events.wrap(window).addEventListener "resize", (event) ->
  page.width = Screen.width
  page.height = Screen.height

  for i in [0..numberOfPage-1]
    pageArray[i].width = Screen.width
    pageArray[i].height = Screen.height
    pageArray[i].x = Screen.width * i






# example to use layers defined in an external file
# module = require './modules/test'
# module.mylayerA.props =
#   x: 0
#   y: 100
# module.mylayerA.superLayer = mainContainerArray[1]





# # loop to generate some random bloc
# for i in [0..5]
#   blocItem = new Layer
#     superLayer: pageArray[0]
#     width: 200
#     height: 500
#     backgroundColor: Utils.randomChoice(palette)
#     y: 505 * i + 5

#   blocItem.centerX()